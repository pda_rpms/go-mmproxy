%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}
%global _enable_debug_package 0

%global selinuxtype targeted
%global moduletype contrib
%if 0%{?rhel}
    %global local_gopath %{_builddir}/%{name}-%{version}/goroot
%else
    %global local_gopath %{gopath}
    %global go_compiller gcc
%endif

Name:		go-mmproxy
Version:	1.0
Release:	2%{?dist}
Summary:	Golang implementation of MMProxy
Summary(ru_RU.UTF-8): Реализация MMProxy на Go
Packager:	Dmitriy Pomerantgsev <pda2@yandex.ru>
License:	BSD
Group:		System Environment/Daemons
URL:		https://github.com/path-network/%{name}
Source0:	https://github.com/path-network/%{name}/archive/%{version}.tar.gz
Source1:	go-mmproxy@.service
Source2:	path-prefixes.txt
Source3:	service.conf.example
Source4:	%{name}.1.gz
Source5:	%{name}.te
Source6:	%{name}.fc
Source7:	%{name}.if
Patch0:		01_c339292c.patch
Patch1:		02_cdac58b2.patch
Patch2:		03_d8cb3ccc.patch
Patch3:		04_6dbb1ceb.patch
Requires:	kernel >= 2.6.28
Requires:	glibc, systemd, iproute, grep
Requires(post): systemd
Requires(postun): systemd
Recommends:	%{name}-selinux
BuildRequires:	golang >= 1.11
BuildRequires:	systemd, binutils
%if "%{go_compiller}" == "gcc"
BuildRequires:	gcc-go
%endif
%if 0%{?rhel}
BuildRequires:	git
%else
BuildRequires:	golang-uber-zap-devel
%endif

%package selinux
Summary:	%{name} selinux
License:	Public Domain
Group:		System Environment/Daemons
Requires:	%{name} = %{version}-%{release}
BuildRequires:	selinux-policy, selinux-policy-devel, bzip2
BuildArch:	noarch
%{?selinux_requires}

%description
This is a Go reimplementation of mmproxy, created to improve on mmproxy's
runtime stability while providing potentially greater performance in terms
of connection and packet throughput.

%{name} is a standalone application that unwraps HAProxy's PROXY protocol
(also adopted by other projects such as NGINX) so that the network connection
to the end server comes from client's - instead of proxy server's - IP address
and port number. Because they share basic mechanisms, Cloudflare's blogpost on
mmproxy serves as a great write-up on how %{name} works under the hood.

%description -l ru_RU.UTF-8
Данная реализация mmproxy на языке Go создана для того, чтобы повышения
стабильности работы, а так же для обеспечения большей производительности
в рамках обработки соединений и пакетов.

%{name} это самостоятельно приложение, извлекающее данные из протокола
PROXY демона HAProxy, так чтобы соединение для клиентов выглядело как
соединение с реальным IP-адресом и портом. Поскольку программа использует
те же принципы, можно ознакомиться с постом Cloudflare для пояснения
принципов работы.

%description selinux
The %{name}-selinux package contains selinux policy for the %{name}
daemon.

%description selinux -l ru_RU.UTF-8
Пакет %{name}-selinux содержит политики selinux для службы %{name}.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .example-typo
%patch1 -p1 -b .v2-halfbyte-parsing
%patch2 -p1 -b .udp-proxying
%patch3 -p1 -b .nil-saddr

%if 0%{?rhel}
    %{__mkdir_p} %{local_gopath}
    pushd %{local_gopath}
    GOPATH="%{local_gopath}" go get -u go.uber.org/zap
    popd
%endif

# selinux
%{__mkdir_p} %{_builddir}/%{name}-%{version}/selinux
%{__cp} -p %{SOURCE5} %{SOURCE6} %{SOURCE7} %{_builddir}/%{name}-%{version}/selinux/

%build
%if "%{go_compiller}" == "gcc"
GOPATH="%{local_gopath}" go build -v -compiler gccgo -gccgoflags "-march=native -O2" -o %{name}
%else
GOPATH="%{local_gopath}" go build -v -o %{name}
%endif
%{__strip} --strip-unneeded %{name}

# selinux
pushd %{_builddir}/%{name}-%{version}/selinux
%{__make} NAME=%{_policytype} -f /usr/share/selinux/devel/Makefile
%{__bzip2} --best %{name}.pp
popd

%install
%{__mkdir_p} %{buildroot}%{_prefix}/bin
%{__install} -pm 0744 %{name} %{buildroot}%{_prefix}/bin/

%{__mkdir_p} %{buildroot}%{_unitdir}/
%{__install} -pm 0644 %{SOURCE1} %{buildroot}%{_unitdir}/

%{__mkdir_p} %{buildroot}%{_sysconfdir}/%{name}
%{__install} -pm 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/%{name}/path-prefixes.txt
%{__install} -pm 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/%{name}/service.conf.example

%{__mkdir_p} %{buildroot}%{_mandir}/man1/
%{__install} -pm 0644 %{SOURCE4} %{buildroot}%{_mandir}/man1/

# selinux
%{__mkdir_p} %{buildroot}%{_datadir}/selinux/packages/%{selinuxtype}
%{__install} -pm 0644 selinux/%{name}.pp.bz2 %{buildroot}%{_datadir}/selinux/packages/%{selinuxtype}
#%{__mkdir_p} %{buildroot}%{_datadir}/selinux/devel/include/%{moduletype}
#%{__install} -pm 0644 selinux/%{name}.if %{buildroot}%{_datadir}/selinux/devel/include/%{moduletype}/%{name}.if

%post
%systemd_post %{name}@.service

%postun
%systemd_postun %{name}@.service

%files
%license LICENSE
%doc README.md path-prefixes.txt
%config(noreplace) %{_sysconfdir}/%{name}/path-prefixes.txt
%config %{_sysconfdir}/%{name}/service.conf.example
%{_prefix}/bin/*
%{_unitdir}/%{name}@.service
%{_mandir}/man1/%{name}.1*

%files selinux
%{_datadir}/selinux/packages/%{selinuxtype}/%{name}.pp.bz2
%ghost %{_sharedstatedir}/selinux/%{selinuxtype}/active/modules/200/%{name}
#{_datadir}/selinux/devel/include/#{moduletype}/#{name}.if

%post selinux
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{selinuxtype}/%{name}.pp.bz2

%postun selinux
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s %{selinuxtype} %{name}
fi

%posttrans selinux
%selinux_relabel_post -s %{selinuxtype}

%changelog
* Tue Mar 31 2020 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0-2
- Package ported from Ubuntu: http://ubuntu.mirror.ac.ke/ubuntu-archive/ubuntu/ubuntu/pool/universe/g/go-mmproxy/
